V =

ifeq ($(strip $(V)),)
        E = @echo
        Q = @
else
        E = @\#
        Q =
endif
export E Q

TARGET      := clk
SRCS        := clk.c
OBJS        := ${SRCS:.c=.o} 

CC       = gcc
WARNINGS = -Wall -Wstrict-prototypes -pedantic
DEBUG    = -g
PKGLIST  = xcb 
CFLAGS   = -O2 $(WARNINGS) $(DEBUG) -W -Wpointer-arith -Wbad-function-cast `pkg-config --cflags $(PKGLIST)`
LFLAGS   = `pkg-config --libs $(PKGLIST)`
LIBS     = 

.PHONY: all clean distclean 
all:: ${TARGET} ${PLUGIN}

${TARGET}: ${OBJS} 
	$(E) "  LINK    " $@
	$(Q) ${CC} ${LFLAGS} -o $@ $^ ${LIBS} 

${OBJS}: %.o: %.c  
	$(E) "  CC      " $@
	$(Q) ${CC} ${CFLAGS} -o $@ -c $< 

clean:: 
	$(E) "  CLEAN"
	$(Q) -rm -f *~ *.o ${TARGET} ${PLUGIN}

distclean:: clean