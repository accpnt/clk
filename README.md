# clk
Initially a fork of [lal](http://comm.it.cx/cgit/lal/), which was a lightweight Xlib standalone clock for system trays. I ported and modded the code from Xlib to XCB. 

Intended to work along with [stalonetray](http://stalonetray.sourceforge.net/). But it should work with any system tray though. 