#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <signal.h>
#include <sys/select.h>
#include <xcb/xcb.h>

/* configuration */
#define DOCKAPP_WIDTH 24
#define DOCKAPP_HEIGHT 24
#define DOCKAPP_FONT "-*-fixed-*-*-*-*-*-*-*-*-*-*-*-*"
#define DOCKAPP_FONT_X 5
#define DOCKAPP_FONT_Y_H 10
#define DOCKAPP_FONT_Y_M 20
#define DOCKAPP_FONT_COLOR_BG 0x1c1c1c00
#define DOCKAPP_FONT_COLOR_FG 0x82828200


/* internals */
#define SYSTEM_TRAY_REQUEST_DOCK  (0)

/* globals */
char *timeformat = "%R";
int running = 0;
xcb_connection_t * c;
xcb_screen_t * s;


/* atom name that we want to find the atom for */
const char atom_tray_name[] = "_NET_SYSTEM_TRAY_OPCODE";

/* function prototypes */
static xcb_gc_t get_font_gc(xcb_window_t w, const char * font_name);
static void draw_text(xcb_window_t w, int16_t x1, int16_t y1, const char * label);
static void test_cookie(xcb_void_cookie_t cookie, char * errmsg);
static void handle_term(int signal);
static void get_time(char * strtime);
static xcb_atom_t retrieve_atom(const char * atom_name);
static int retrieve_tray_id(int screen_num);
static void draw_clk(xcb_window_t w);

static void test_cookie(xcb_void_cookie_t cookie, char * errmsg)
{
    xcb_generic_error_t *error = xcb_request_check(c, cookie);
    if (error) {
        fprintf(stderr, "ERROR: %s : %"PRIu8"\n", errmsg , error->error_code);
        xcb_disconnect(c);
        exit(-1);
    }
}

static void draw_text(xcb_window_t w, int16_t x1, int16_t y1, const char * label)
{
    /* get graphics context */
    xcb_gcontext_t gc = get_font_gc(w, DOCKAPP_FONT);

    /* draw the text */
    xcb_void_cookie_t text_cookie = xcb_image_text_8_checked(c, strlen(label), w, gc, x1, y1, label);
    test_cookie(text_cookie, "can't paste text");

    /* free the gc */
    xcb_void_cookie_t gc_cookie = xcb_free_gc(c, gc);
    test_cookie(gc_cookie, "can't free gc");
}


static xcb_gc_t get_font_gc(xcb_window_t w, const char *font_name)
{
    /* get font */
    xcb_font_t font = xcb_generate_id(c);
    xcb_void_cookie_t font_cookie = xcb_open_font_checked(c, font, strlen(font_name), font_name);
    test_cookie(font_cookie, "can't open font");

    /* create graphics context */
    xcb_gcontext_t  gc            = xcb_generate_id(c);
    uint32_t        mask          = XCB_GC_FOREGROUND | XCB_GC_BACKGROUND | XCB_GC_FONT;
    uint32_t        value_list[3] = {DOCKAPP_FONT_COLOR_FG, DOCKAPP_FONT_COLOR_BG, font};

    xcb_void_cookie_t gc_cookie = xcb_create_gc_checked(c, gc, w, mask, value_list);
    test_cookie(gc_cookie, "can't create gc");

    /* close font */
    font_cookie = xcb_close_font_checked(c, font);
    test_cookie(font_cookie, "can't close font");

    return gc;
}

static void handle_term(int signal)
{
    running = 0;
}

static void get_time(char *strtime)
{
    time_t curtime;
    struct tm *curtime_tm;

    curtime = time(NULL);
    curtime_tm = localtime(&curtime);
    strftime(strtime, 49, timeformat, curtime_tm);
}

static xcb_atom_t retrieve_atom(const char * atom_name)
{
    xcb_atom_t atom;
    xcb_intern_atom_cookie_t atom_cookie;
    xcb_intern_atom_reply_t * atom_reply;

    atom_cookie = xcb_intern_atom(c, 0, strlen(atom_name), atom_name);
    atom_reply  = xcb_intern_atom_reply(c, atom_cookie, NULL);
    atom        = atom_reply->atom;
    printf("INFO: %s : %d\n", atom_name, atom);
    free(atom_reply);

    return atom;
}

static int retrieve_tray_id(int screen_num)
{
    long int tray_id = 0;
    char atom_name[strlen("_NET_SYSTEM_TRAY_S") + 11];

    snprintf(atom_name, strlen("_NET_SYSTEM_TRAY_S") + 11, "_NET_SYSTEM_TRAY_S%d", screen_num);
    printf("%s\n", atom_name);
    xcb_intern_atom_cookie_t selection_cookie = xcb_intern_atom(c, 0, strlen(atom_name), atom_name);
    xcb_intern_atom_reply_t *selection_reply = xcb_intern_atom_reply(c, selection_cookie, NULL);
    xcb_atom_t selection = selection_reply->atom;
    xcb_get_selection_owner_cookie_t cookie = xcb_get_selection_owner(c, selection);
    xcb_get_selection_owner_reply_t *selection_owner = xcb_get_selection_owner_reply(c, cookie, NULL);
    if(selection_owner && selection_owner->owner) {
        printf("INFO: current tray id : %ld\n", (long int)selection_owner->owner);
        tray_id = selection_owner->owner;
        free(selection_owner);
    }

    return tray_id;
}

static void draw_clk(xcb_window_t w)
{
    char strtime[50];
    char hours[3];
    char minutes[3];

    xcb_clear_area(c, 0, w, 0, 0, DOCKAPP_WIDTH, DOCKAPP_HEIGHT);
    get_time(strtime);
    memset(hours, 0, sizeof(hours));
    memset(minutes, 0, sizeof(minutes));
    hours[0]   = strtime[0];
    hours[1]   = strtime[1];
    minutes[0] = strtime[3];
    minutes[1] = strtime[4];
    draw_text(w, DOCKAPP_FONT_X, DOCKAPP_FONT_Y_H, hours);
    draw_text(w, DOCKAPP_FONT_X, DOCKAPP_FONT_Y_M, minutes);

    xcb_flush(c);
}

/* request a window to be docked on the tray */
void request_tray(long int tray_id, long int window_id, xcb_atom_t atom)
{
    xcb_client_message_event_t tray_request;

    tray_request.response_type  = XCB_CLIENT_MESSAGE;
    tray_request.format         = 32;
    tray_request.sequence       = 0;
    tray_request.window         = tray_id;  // tray window id
    tray_request.type           = atom;  // TODO : change hardcode
    tray_request.data.data32[0] = XCB_CURRENT_TIME;
    tray_request.data.data32[1] = SYSTEM_TRAY_REQUEST_DOCK;
    tray_request.data.data32[2] = window_id;  // window id to be docked 

    xcb_send_event(c, 0, tray_id, XCB_EVENT_MASK_NO_EVENT, (const char *)&tray_request);
}

int main(int argc, char * argv[])
{
    int rc = 0;

    /* Time stuff */
    int xfd = 0;
    int selectret = 0;
    fd_set fds;
    struct timeval timeout;

    int redraw;
    xcb_atom_t atom;
    long int tray_id;

    /* signals handling */
    struct sigaction act;

    act.sa_handler = &handle_term;
    sigaction(SIGTERM, &act, NULL);
    sigaction(SIGINT, &act, NULL);
    running = 1;


    /* get the connection */
    int screen_num;
    c = xcb_connect(NULL, &screen_num);
    if(!c) {
        fprintf(stderr, "ERROR: can't connect to an X server\n");
        rc = -1;
        goto exit;
    }

    /* get the current screen */
    xcb_screen_iterator_t iter = xcb_setup_roots_iterator(xcb_get_setup (c));
    /* we want the screen at index screen_num of the iterator */
    for(int i = 0; i < screen_num; ++i) {
        xcb_screen_next(&iter);
    }

    s = iter.data;
    if(!s) {
        fprintf(stderr, "ERROR: can't get the current screen\n");
        rc = -1;
        goto clean;
    }

    /* retrieve tray info */
    tray_id = retrieve_tray_id(screen_num);
    atom    = retrieve_atom(atom_tray_name); 

    /* create the window */
    xcb_window_t w = xcb_generate_id(c);

    uint32_t mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
    uint32_t values[2];
    values[0] = DOCKAPP_FONT_COLOR_BG;
    values[1] = XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_POINTER_MOTION;

    xcb_void_cookie_t window_cookie = xcb_create_window_checked(c, s->root_depth, w, tray_id, 0, 0, DOCKAPP_WIDTH, DOCKAPP_HEIGHT, 0, XCB_WINDOW_CLASS_INPUT_OUTPUT, s->root_visual, mask, values);
    test_cookie(window_cookie, "can't create window");
    printf("INFO: window id : %u (0x%x)\n", w, w);

    xcb_void_cookie_t map_cookie = xcb_map_window_checked(c, w);
    test_cookie(map_cookie, "can't map window");

    /* dock the window */
    request_tray(tray_id, w, atom);

    xcb_flush(c);

    timeout.tv_sec = timeout.tv_usec = 0;

    /* event loop */
    xcb_generic_event_t * event;
    while(running) {
        redraw = 0;

        FD_ZERO(&fds);
        FD_SET(xfd, &fds);

        selectret = select(xfd+1, &fds, NULL, NULL, &timeout);

        switch(selectret) {
        case -1:
            continue;
        case 0:
            redraw = 1;
            timeout.tv_sec = 1;
            timeout.tv_usec = 0;
        case 1:
            if(FD_ISSET(xfd, &fds)) {
                if((event = xcb_poll_for_event(c))) {
                    switch (event->response_type & ~0x80) {
                    case XCB_EXPOSE:
                        redraw = 1;
                        break;
                    case XCB_KEY_RELEASE:
                    default:
                        break;
                    }
                }

                free(event);
            }
        default: 
            break;
        }

        if(1 == redraw) {
            draw_clk(w);
        }
    }

clean:
    xcb_disconnect(c);
exit:
    fprintf(stderr, "INFO: exiting, rc = %d\n", rc);
    return rc;
}
