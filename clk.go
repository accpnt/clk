package main

import (
	"fmt"
	"time"
	"syscall"
	"os"
	"os/signal"
	"github.com/BurntSushi/xgb"
	"github.com/BurntSushi/xgb/xproto"
)

const (
	width = 24
	height = 24
	clk_font = "-*-fixed-*-*-*-*-*-*-*-*-*-*-*-*"
	font_x_offset = 5
	font_y_offset_h = 10
	font_y_offset_m = 20
	font_color_bg = 0xffffffff
	font_color_fg = 0x00000000
	timeformat = "%R"
	atom_tray_name = "_NET_SYSTEM_TRAY_OPCODE"
	atom_system_tray_name = "__NET_SYSTEM_TRAY_S"
	system_tray_request_dock = 0
)

var (
	running bool
	c * xgb.Conn  // connection
)

func get_font_gc(w xproto.Window, font_name string) xproto.Gcontext {
	// get font
	fmt.Println(w, font_name)
	Font, err := xproto.NewFontId(c)
	fmt.Println(err)
	xproto.OpenFont(c, Font, uint16(len(font_name)), font_name)

	// create graphics context
	gc, err	:= xproto.NewGcontextId(c)
	mask := xproto.GcForeground | xproto.GcBackground | xproto.GcFont
	value_list := []uint32{font_color_fg, font_color_bg, uint32(Font)}

	xproto.CreateGCChecked(c, gc, xproto.Drawable(w), uint32(mask), value_list).Check()
	if err != nil {
		fmt.Printf("error creating GC: %s\n", err)
	} else { // neva
		fmt.Printf("created GC successfully\n")
	}

	// close font
	xproto.CloseFont(c, Font)

	return gc
}

func draw_text(w xproto.Window, x1 int16, y1 int16, label string) {
	// get graphics context
	gc := get_font_gc(w, clk_font)

	// draw the text
	xproto.ImageText8(c, byte(len(label)), xproto.Drawable(w), gc, x1, y1, label)

	/* free the gc */
	xproto.FreeGC(c, gc)
}

func handle_term() {
	fmt.Println("exiting...")
	running = false
}

func retrieve_atom(atom_name string) xproto.Atom {
	atom_cookie := xproto.InternAtom(c, false, uint16(len(atom_name)), atom_name)
	atom_reply, err := atom_cookie.Reply()
	if err != nil {
		fmt.Printf("error replying atom: %s\n", err)
	} else { 
		fmt.Printf("%s = %u\n", atom_name, atom_reply.Atom)
	}
	atom := atom_reply.Atom

	return atom
}

func retrieve_tray_id(screen_num int32) int32 {
	var tray_id int32

	atom_name := fmt.Sprintf("%s%d", atom_system_tray_name, screen_num)

	fmt.Println(atom_name, screen_num, uint16(len(atom_name)))

	selection_cookie := xproto.InternAtom(c, false, uint16(len(atom_name)), atom_name)
	selection_reply, err := selection_cookie.Reply()
	selection := selection_reply.Atom
	selection_owner_cookie := xproto.GetSelectionOwner(c, selection)
	selection_owner, err := selection_owner_cookie.Reply()
	if err != nil {
		fmt.Printf("error replying atom: %s\n", err)
	} else { 
		fmt.Printf("retrieve_tray_id: %s = %u\n", atom_name, selection_owner.Owner)
	}

	tray_id = int32(selection_owner.Owner)

	return tray_id
}

func draw_clk(w xproto.Window) {
	t := time.Now()
	strtime := t.Format("15:04")
	var hours = strtime[0:1]
	var minutes = strtime[3:4]

	fmt.Println(strtime)

	err := xproto.ClearAreaChecked(c, false, w, 0, 0, width, height).Check()
	if err != nil {
		fmt.Printf("error clearing area: %s\n", err)
	} else { // neva
		fmt.Printf("area cleared successfully\n")
	}

	draw_text(w, font_x_offset, font_y_offset_h, hours)
	draw_text(w, font_x_offset, font_y_offset_m, minutes)
}

// request a window to be docked on the tray
func request_tray(tray_id int32, window_id xproto.Window, atom xproto.Atom) {
	fmt.Println("requesting tray...", tray_id)
	var tray_request xproto.ClientMessageEvent

	tray_request.Format = 32
	tray_request.Sequence = 0
	tray_request.Window = xproto.Window(tray_id)
	tray_request.Type = atom
	tray_request.Data.Data32 = []uint32 {
			xproto.TimeCurrentTime,
			system_tray_request_dock, 
			uint32(window_id)}	

	xproto.SendEvent(c, false, xproto.Window(tray_id), xproto.EventMaskNoEvent, tray_request.String())
}


func FD_SET(p *syscall.FdSet, i int) {
	p.Bits[i/64] |= 1 << uint(i) % 64
}

func FD_ISSET(p *syscall.FdSet, i int) bool {
	return (p.Bits[i/64] & (1 << uint(i) % 64)) != 0
}

func FD_ZERO(p *syscall.FdSet) {
	for i := range p.Bits {
		p.Bits[i] = 0
	}
}

func main() {
	var tray_id int32
	var redraw bool
	fds := &syscall.FdSet{}
	var xfd int
	var atom xproto.Atom

	// time stuff
	timeout := &syscall.Timeval{}
	running = true

	// signals handling
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)
    go func() {
        <-signals
        handle_term()
        os.Exit(1)
    }()

	// get the connection
	connection, err := xgb.NewConn()
	if err != nil {
		fmt.Println(err)
		return
	}

	c = connection

	w, _ := xproto.NewWindowId(c)
	s := xproto.Setup(c).DefaultScreen(c)  // returns a ScreenInfo

	// retrieve tray info
    tray_id = retrieve_tray_id(0)
    fmt.Println("tray_id =", tray_id)
    atom    = retrieve_atom(atom_tray_name) 

	xproto.CreateWindow(c, s.RootDepth, w, s.Root, 0, 0, width, height, 0,
		xproto.WindowClassInputOutput, s.RootVisual,
		xproto.CwBackPixel | xproto.CwEventMask,
		[]uint32{ // values must be in the order defined by the protocol
			font_color_bg,
			xproto.EventMaskStructureNotify})

	err = xproto.MapWindowChecked(c, w).Check()
	if err != nil {
		fmt.Printf("Checked Error for mapping window 0x1: %s\n", err)
	} else { // neva
		fmt.Printf("Map window 0x1 successful!\n")
	}

	// dock the window
	request_tray(tray_id, w, atom)
	fmt.Println(atom, tray_id)
 
	for running {
		FD_ZERO(fds)
		FD_SET(fds, xfd)
		selectret, err := syscall.Select(xfd+1, fds, nil, nil, timeout)
		if err != nil {
			break
		}

		switch selectret {
		case -1:
			continue
		case 0:
			redraw = true
			timeout.Sec = 1
			timeout.Usec = 0
		case 1:
			if FD_ISSET(fds, xfd) {
				ev, xerr := c.WaitForEvent()
				if ev == nil && xerr == nil {
					fmt.Println("Both event and error are nil. Exiting...")
					return
				}

				if ev != nil {
					fmt.Printf("Event: %s\n", ev)
				}
				if xerr != nil {
					fmt.Printf("Error: %s\n", xerr)
				}

				switch ev.Bytes()[0] {
				case xproto.Expose:
					redraw = true
				default: 
					break
				}
			}
			break
		default: 
			break
		}

		if(true == redraw) {
			draw_clk(w)
		}
	}

	return
}
